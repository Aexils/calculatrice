//
//  main.swift
//  Hello World
//
//  Created by Alexis Levasseur on 16/04/2020.
//  Copyright © 2020 Alexis Levasseur. All rights reserved.
//

func input() -> Int {
    let strData = readLine();

    return Int(strData!)!
}

func calculator() {
    print("--- Menu ---")
    print("1. Addition")
    print("2. Soustraction")
    print("3. Multiplication")
    print("4. Division")
    print("5. Modulo")
    print("6. Carré")

    print("Que voulez-vous ? (de 1 à 5)")
    var nombreEntre: Int
    nombreEntre = input()

    switch nombreEntre {
    case 1:
        print("Veuillez choisir un premier chiffre à additioner")
        var addition1: Int
        addition1 = input()
        print("Vous avez choisis \(addition1) quel est votre deuxième chiffre?")
        var addition2: Int
        addition2 = input()
        var addition3 = addition1 + addition2
        print("\(addition1) + \(addition2) = \(addition3)")
    case 2:
        print("Veuillez choisir un premier chiffre à soustraire")
        var soustraction1: Int
        soustraction1 = input()
        print("Vous avez choisis \(soustraction1) quel est votre deuxième chiffre?")
        var soustraction2: Int
        soustraction2 = input()
        var soustraction3 = soustraction1 - soustraction2
        print("\(soustraction1) - \(soustraction2) = \(soustraction3)")
    case 3:
        print("Veuillez choisir un premier chiffre à multiplier")
        var multiplication1: Int
        multiplication1 = input()
        print("Vous avez choisis \(multiplication1), quel est votre deuxième chiffre?")
        var multiplication2: Int
        multiplication2 = input()
        var multiplication3 = multiplication1 * multiplication2
        print("\(multiplication1) * \(multiplication2) = \(multiplication3)")
    case 4:
        print("Veuillez choisir un premier chiffre à diviser")
        var division1: Int
        division1 = input()
        print("Vous avez choisis \(division1), quel est votre deuxième chiffre?")
        var division2: Int
        division2 = input()
        var division3 = division1 / division2
        print("\(division1) / \(division2) = \(division3)")
    case 5:
        print("Veuillez choisir un premier chiffre pour avoir le reste")
        var modulo1: Int
        modulo1 = input()
        print("Vous avez choisis \(modulo1), quel est votre deuxième chiffre?")
        var modulo2: Int
        modulo2 = input()
        var modulo3 = modulo1 % modulo2
        print("\(modulo1) % \(modulo2) = \(modulo3)")
    case 6:
        print("Veuillez choisir un chiffre pour avoir son carré")
        var carre1: Int
        carre1 = input()
        print("Vous avez choisis \(carre1)")
        var carre2 = carre1 * carre1
        print("\(carre1) * \(carre1) = \(carre2)")
    default:
        print("Erreur, un chiffre entre 1 et 5 est demandé !")
    }

    retry()
}

func retry() {
    print("Voulez-vous faire un nouveau calcul?")
    print("1. Oui")
    print("2. Non")
    var response: Int
    response = input()
    if response == 1 {
        calculator()
    } else {
        print("Merci et bonne journée")
    }
}

calculator()